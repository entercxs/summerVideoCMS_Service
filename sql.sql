-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        5.7.17-log - MySQL Community Server (GPL)
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  9.5.0.5277
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- 导出 summervideocms 的数据库结构
CREATE DATABASE IF NOT EXISTS `summervideocms` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `summervideocms`;

-- 导出  表 summervideocms.admin 结构
CREATE TABLE IF NOT EXISTS `admin` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `admin_id` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `token_id` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `userName` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `createTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `admin_token_id` (`token_id`),
  CONSTRAINT `admin_token_id` FOREIGN KEY (`token_id`) REFERENCES `token` (`token_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- 正在导出表  summervideocms.admin 的数据：~1 rows (大约)
DELETE FROM `admin`;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` (`id`, `admin_id`, `token_id`, `userName`, `email`, `password`, `createTime`) VALUES
	(2, '519f33d7bc419e1b71ca817418f0e07899', 'e0df039e5becbf136a35184cb59bf53222', 'admin', 'admin@admin.com', '901cb50583f89676240b4463ce1d31830ab353eb61136308063b40121898eb5006b47c63da46bccc555e719a74e0164a1313ade5f0879a5e631b5d2c36943277', '2018-05-01 15:35:48');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;

-- 导出  表 summervideocms.token 结构
CREATE TABLE IF NOT EXISTS `token` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `token_id` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT 'token值',
  `createTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `token_id` (`token_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- 正在导出表  summervideocms.token 的数据：~2 rows (大约)
DELETE FROM `token`;
/*!40000 ALTER TABLE `token` DISABLE KEYS */;
INSERT INTO `token` (`id`, `token_id`, `token`, `createTime`, `updateTime`) VALUES
	(13, 'e0df039e5becbf136a35184cb59bf532', 'fcf6f923becb2e529f02ed515ccfba82', '2018-05-01 15:35:48', '2018-05-01 15:36:29'),
	(14, 'e0df039e5becbf136a35184cb59bf53222', '0c67ab788807c2c5e8ffef84599bd518', '2018-05-01 15:35:48', '2018-05-01 15:38:55');
/*!40000 ALTER TABLE `token` ENABLE KEYS */;

-- 导出  表 summervideocms.user 结构
CREATE TABLE IF NOT EXISTS `user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `token_id` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `userName` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `createTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `user_token_id` (`token_id`),
  CONSTRAINT `user_token_id` FOREIGN KEY (`token_id`) REFERENCES `token` (`token_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- 正在导出表  summervideocms.user 的数据：~1 rows (大约)
DELETE FROM `user`;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `user_id`, `token_id`, `userName`, `email`, `password`, `createTime`) VALUES
	(12, '519f33d7bc419e1b71ca817418f0e078', 'e0df039e5becbf136a35184cb59bf532', 'admin', 'admin@admin.com', '901cb50583f89676240b4463ce1d31830ab353eb61136308063b40121898eb5006b47c63da46bccc555e719a74e0164a1313ade5f0879a5e631b5d2c36943277', '2018-05-01 15:35:48');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- 导出  表 summervideocms.verify 结构
CREATE TABLE IF NOT EXISTS `verify` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `verify_id` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `emailCode` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '发送给客户端的验证码',
  `emailCode_createTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '邮件验证码创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- 正在导出表  summervideocms.verify 的数据：~1 rows (大约)
DELETE FROM `verify`;
/*!40000 ALTER TABLE `verify` DISABLE KEYS */;
/*!40000 ALTER TABLE `verify` ENABLE KEYS */;

-- 导出  表 summervideocms.video 结构
CREATE TABLE IF NOT EXISTS `video` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `video_id` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `video_category_id` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` decimal(10,2) unsigned DEFAULT NULL,
  `srcName` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '视频资源名称',
  `srcLocation` text COLLATE utf8mb4_bin NOT NULL COMMENT '视频资源存放的位置',
  `createTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `video_video_category_id` (`video_category_id`),
  KEY `video_id` (`video_id`),
  CONSTRAINT `video_video_category_id` FOREIGN KEY (`video_category_id`) REFERENCES `video_category` (`video_category_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23774 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- 正在导出表  summervideocms.video 的数据：~0 rows (大约)
DELETE FROM `video`;
/*!40000 ALTER TABLE `video` DISABLE KEYS */;
/*!40000 ALTER TABLE `video` ENABLE KEYS */;

-- 导出  表 summervideocms.video_category 结构
CREATE TABLE IF NOT EXISTS `video_category` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `video_category_id` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `video_category_id` (`video_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- 正在导出表  summervideocms.video_category 的数据：~0 rows (大约)
DELETE FROM `video_category`;
/*!40000 ALTER TABLE `video_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `video_category` ENABLE KEYS */;

-- 导出  表 summervideocms.video_comment 结构
CREATE TABLE IF NOT EXISTS `video_comment` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `video_id` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `content` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '评论内容',
  `createTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `video_comment_user_id` (`user_id`),
  KEY `video_comment_video_id` (`video_id`),
  CONSTRAINT `video_comment_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `video_comment_video_id` FOREIGN KEY (`video_id`) REFERENCES `video` (`video_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- 正在导出表  summervideocms.video_comment 的数据：~0 rows (大约)
DELETE FROM `video_comment`;
/*!40000 ALTER TABLE `video_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `video_comment` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
