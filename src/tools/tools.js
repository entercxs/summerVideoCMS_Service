"use strict";
//封装的一些开发冲经常用到的方法
const crypto = require('crypto'),
    fs = require('fs'),
    path = require('path');
module.exports = {

    //数据类型判断
    async isType(data) {
        const ready = await Object.prototype.toString.call(data);
        switch (ready) {
            case '[object Object]':
                return 'Object';
            case '[object Array]':
                return 'Array';
            case '[object String]':
                return 'String';
            case '[object Number]':
                if (Number.isNaN(data)) {
                    return 'NaN';
                } else {
                    return 'Number';
                }
            case '[object Boolean]':
                return 'Boolean';
            case '[object Undefined]':
                return 'Undefined';
            case '[object Null]':
                return 'Null';
            case '[object Uint8Array]':
                return 'Uint8Array';
            case '[object Map]':
                return 'Map';
            default:
                return ready;
        }
    },

    async getMd5(data) {
        return await crypto.createHash('md5').update(data, 'utf8').digest('hex');
    },

    async md5(data, secretKey) {
        return await crypto.createHmac('md5', secretKey).update(data, 'utf8').digest('hex');
    },

    async sha256(data, secretKey) {
        return await crypto.createHmac('sha256', secretKey).update(data, 'utf8').digest('hex');
    },

    async sha512(data, secretKey) {
        return await crypto.createHmac('sha512', secretKey).update(data, 'utf8').digest('hex');
    },

    async aes192(encrypt, secretKey) {
        const cipher = await crypto.createCipher('aes192', secretKey);
        let encrypted = await cipher.update(encrypt, 'utf8', 'hex');
        return encrypted += await cipher.final('hex');
    },

    async aes192Dec(encrypted, secretKey) {
        const decipher = await crypto.createDecipher('aes192', secretKey);
        let decrypted = await decipher.update(encrypted, 'hex', 'utf8');
        return decrypted += await decipher.final('utf8');
    },

    async aes256(encrypt, secretKey) {
        const cipher = await crypto.createCipher('aes256', secretKey);
        let encrypted = await cipher.update(encrypt, 'utf8', 'hex');
        return encrypted += await cipher.final('hex');
    },

    async aes256Dec(encrypted, secretKey) {
        const decipher = await crypto.createDecipher('aes256', secretKey);
        let decrypted = await decipher.update(encrypted, 'hex', 'utf8');
        return decrypted += await decipher.final('utf8');
    },

    async filesHex(fileData) {
        return await crypto.createHash('sha512').update(data, 'utf8').digest('hex');
    },

    //递归创建目录
    async creatdirs(dirsPath) {
        if (await fs.existsSync(dirsPath)) {
            return true;
        } else {
            if (await this.creatdirs(path.dirname(dirsPath))) {
                try {
                    await fs.mkdirSync(dirsPath);
                } catch (err) {
                }
                return true;
            }
        }
    }
};