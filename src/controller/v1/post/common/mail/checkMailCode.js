const Base = require(think.APP_PATH + '/controller/base.js');
module.exports = class extends Base {
    async indexAction() {
        if (think.isEmpty(await this.model('verify/verify').checkMailcode(this.ctx.state.data.data))) {
            this.ctx.state.result = 0
        } else {
            this.ctx.state.result = 1
        }
    };
};