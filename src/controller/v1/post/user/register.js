const Base = require(think.APP_PATH + '/controller/base.js');
module.exports = class extends Base {

    async indexAction() {
        let error;
        let result;
        let {verify_id, emailCode, email, userName, password} = this.ctx.state.data.data;
        password = await think.$tools.sha512(password, 'c3f6$^$$^&6b015 a1849');
        try {

            if (think.isEmpty(await this.model('verify/verify').checkMailcode({verify_id, emailCode, email}))) {

            } else {
                result = await this.model('user/user').register({email, userName, password});
            }

        } catch (err) {
            error = err.message
        }

        this.ctx.state.result = error || {
            userName: result.userName,
            token: result.token,
            user_id: result.user_id
        };
    };

};