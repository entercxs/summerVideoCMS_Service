const Base = require(think.APP_PATH + '/controller/base.js');
module.exports = class extends Base {

    async indexAction() {
        this.ctx.state.result = this.model('admin/admin').login(this.ctx.state.data.data);
    };

};