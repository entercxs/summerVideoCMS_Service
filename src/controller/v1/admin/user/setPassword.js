const Base = require(think.APP_PATH + '/controller/base.js');
module.exports = class extends Base {
    async indexAction() {
        let {admin_id, password} = this.ctx.state.data.data;
        this.model('admin/admin').setPassword({admin_id, password});
        this.ctx.state.result = 1
    };
};