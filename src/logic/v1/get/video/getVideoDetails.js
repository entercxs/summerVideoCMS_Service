module.exports = class extends think.Logic {
    async __before() {
        const pathName = this.ctx.path.slice(this.ctx.path.lastIndexOf('/') + 1);
        this.ctx.state.data = JSON.stringify(this.ctx.post()) !== '{}' ? this.ctx.post() : this.ctx.param();
        this.ctx.state.pathName = pathName;
    }

    async indexAction() {

    }
};