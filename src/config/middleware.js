const path = require('path');
const cors = require('@koa/cors');
const isDev = think.env === 'development';

module.exports = [
    {
        handle: cors,
        options: {
            origin: '*',
            allowHeaders: '*',
            exposeHeaders: '*',
            credentials: true,
            maxAge: 86400,
            allowMethods: 'GET,HEAD,PUT,POST,DELETE,PATCH'
        }
    },
    {
        handle: 'meta',
        options: {
            logRequest: isDev,
            sendResponseTime: isDev
        }
    },
    {
        handle: 'resource',
        enable: true,
        options: {
            root: path.join(think.ROOT_PATH, 'www'),
            publicPath: /^\/(static|favicon\.ico)/
        }
    },
    {
        handle: 'trace',
        enable: !think.isCli,
        options: {
            debug: isDev
        }
    },
    {
        handle: 'payload',
        options: {
            keepExtensions: true,
            limit: '9999999mb',
            maxFieldsSize: '9999999mb',
            hash: 'md5',
            multiples: true,
            uploadDir: path.join(think.ROOT_PATH, 'runtime/upload/'),
        }
    },
    {
        handle: 'router',
        options: {}
    },
    'logic',
    'controller'
];
