const str = Math.random().toString(36).substr(2);
const time = new Date().getTime();
module.exports = class extends think.Model {
    async getList() {
        return this.select();
    }

    async deleteCategory(data) {
        return this.where(data).delete();
    }

    async addvCategory(data) {
        data.video_category_id = await think.$tools.getMd5(str + time + Math.random());
        return this.add(data)
    }
};