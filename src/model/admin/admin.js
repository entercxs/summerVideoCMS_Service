const str = Math.random().toString(36).substr(2);
const time = new Date().getTime();
module.exports = class extends think.Model {

    async login(data) {
        data.password = await think.$tools.sha512(data.password, 'c3f6$^$$^&6b015 a1849');
        const userInfo = await this.where(data).select();
        if (think.isEmpty(userInfo)) {
            throw false
        } else {
            const token = await this.model('token/token').updateToken(userInfo);
            token.admin_id = JSON.parse(JSON.stringify(userInfo))[0].admin_id;
            return token
        }
    }

    async setPassword(data) {
        data.password = await think.$tools.sha512(data.password, 'c3f6$^$$^&6b015 a1849');
        return this.where({admin_id: data.admin_id}).update({password: data.password});
    }

    async checkUserToken(data) {
        return this.alias('table_a').join({
            admin: {
                on: {
                    admin_id: 'admin_id',
                }
            },
            token: {
                as: 'table_b',
                on: {
                    token_id: 'token_id'
                }
            },
        }).where({'table_b.token': data.token, 'table_a.admin_id': data.admin_id}).find();
    }


};
