module.exports = class extends think.Model {
    async addMailcode(data) {
        this.add(data);
    }

    async checkMailcode(data) {
        return this.where(data).find();
    }
};