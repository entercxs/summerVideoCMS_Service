const str = Math.random().toString(36).substr(2);
const time = new Date().getTime();
module.exports = class extends think.Model {
    async getList(data) {

        const data2 = {
            orderByKey: 'createTime',
            orderByType: 'DESC',
        };

        const page = data.page;

        delete data.page;

        const result = await this.alias('table_main').field('table_user.userName,comment_id,content,createTime,user_id').join({
            user: {
                as: 'table_user',
                join: 'left',
                on: {
                    user_id: 'user_id',
                }
            }
        }).where({'table_main.video_id': data.video_id}).order(`${data2.orderByKey} ${data2.orderByType}`).page(page, 12).countSelect();

        return result
    }

    async save(data) {
        const checkUser = await this.model('user/user').checkUserToken(data);

        if (think.isEmpty(checkUser)) {
            throw false
        }

        data.comment_id = await think.$tools.getMd5(str + time + Math.random());

        return this.add(data)
    }
};