const str = Math.random().toString(36).substr(2);
const time = new Date().getTime();
module.exports = class extends think.Model {

    async addToken() {
        const data = {
            token_id: await think.$tools.getMd5(str + time + Math.random()),
            token: await think.$tools.getMd5(str + time + Math.random())
        };
        this.add(data);
        return data;
    }


    async updateToken(userInfo) {
        const token_id = userInfo[0].token_id;
        const token = await think.$tools.getMd5(str + time + Math.random());
        this.where({token_id}).update({token});
        return {token}
    }

};
