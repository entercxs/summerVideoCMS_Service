const str = Math.random().toString(36).substr(2);
const time = new Date().getTime();
module.exports = class extends think.Model {
    async getList(data) {
        const data2 = {
            orderByKey: 'updateTime',
            orderByType: 'DESC',
        };
        const page = data.page;
        delete data.page;
        return this.where(data).order(`${data2.orderByKey} ${data2.orderByType}`).page(page, 12).countSelect();
    }

    async deleteVideo(data) {
        return this.where(data).delete();
    }

    async addVideo(data) {
        data.video_id = await think.$tools.getMd5(str + time + Math.random());
        return this.add(data)
    }
};