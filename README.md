技术Q群:187816412

## 环境要求
```
node : 10+
mysql : 5.7+

node安装方法请参考:https://github.com/nodesource/distributions
数据库自行百度,建议安装宝塔面板
```

## 配置说明
```
文件位置:src/config/adapter.js
找到:
  mysql: {
    handle: mysql,
    database: 'summervideocms',//数据库名
    prefix: '',
    encoding: 'utf8mb4_bin',
    host: '127.0.0.1',//数据库位置,默认本地
    port: '3306',//端口
    user: 'summervideocms',//用户名
    password: 'summervideocms',//密码
    dateStrings: true
  }
  在你的数据库,例如summervideocms数据库中导入sql.sql脚本,在修改连接信息即可

  文件位置:src/controller/v1/post/common/mail/sendMailCode.js
  找到:
        const mailOptions = {
            from: 'postmaster <postmaster@admin.com>',//发件人地址
            to: field.email,
            subject: 'postmaster',//主题
            html: `<p>${field.emailCode}</p>`
        };

        const transporter = nodemailer.createTransport({
            host: 'email-smtp.us-west-2.amazonaws.com',//smtp服务器地址
            port: 465,//端口
            secure: true,//如果是加密的端口为true,否则false
            auth: {
                user: 'postmaster',//登陆smtp服务器的账号
                pass: 'postmaster'//登陆smtp服务器的密码
            }
        });
  邮件配置失败,则用户无法注册
```

## 部署方法
```
1.下载本程序至服务器并解压
2.在解压的根目录下执行下面两条命令
    npm install -g cnpm --registry=https://registry.npm.taobao.org (如已经装cnpm,可跳过这条命令)
    cnpm i
3.安装pm2
    cnpm install -g pm2 (如已安装pm2可跳过)
4.启动
    npm run pm2
    pm2 startup
    pm2 save
5.在浏览器打开 你的域名或者ip + ':7001' 如 www.name.com:7001 可正常打开网页,即部署成功
6.默认信息
    管理员账号: admin@admin.com              admin
    普通用户账号: admin@admin.com              admin
    请自行更改
    前端项目需要单独部署:https://gitee.com/newcomein/summerVideoCMS_PC
    管理界面需要单独部署:https://gitee.com/newcomein/summerVideoCMS_Admin
```

## 捐赠
```
😂很穷,没钱了,求捐赠:
支付宝:rock543400@qq.com
```